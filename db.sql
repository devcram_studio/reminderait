DROP TABLE IF EXISTS users;

CREATE TABLE users (
	email VARCHAR(50),
	fullName VARCHAR(50),
	PRIMARY KEY(email) );

/*-------------------------------*/

DROP TABLE IF EXISTS friends;

CREATE TABLE friends (

	user1 VARCHAR(50) ,
	user2 VARCHAR(50),
	PRIMARY KEY(user1,user2),
	FOREIGN KEY (user1) REFERENCES users (email),
	FOREIGN KEY (user2) REFERENCES users (email));

/*-------------------------------*/

DROP TABLE IF EXISTS reminders;

CREATE TABLE reminders (
	id INTEGER AUTO_INCREMENT ,
	subject VARCHAR (50),
	latitude DOUBLE,
	longitude DOUBLE,
	radius INT,
	owner VARCHAR(50),
	creator VARCHAR(50),
	PRIMARY KEY(id),
	FOREIGN KEY (owner) REFERENCES users (email),
	FOREIGN KEY (creator) REFERENCES users (email)	);
