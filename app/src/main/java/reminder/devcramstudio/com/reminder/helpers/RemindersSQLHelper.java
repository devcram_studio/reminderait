package reminder.devcramstudio.com.reminder.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;

import reminder.devcramstudio.com.reminder.model.Reminder;

/**
 * Created by nico on 4/10/15.
 */
public class RemindersSQLHelper extends SQLiteOpenHelper{
    private static final String DB_NAME = "remindersAIT.db";
    private static final String TABLE_NAME = "remindersAIT";


    public RemindersSQLHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_NAME + " (id INTEGER auto_increment PRIMARY KEY, geofence_id VARCHAR(50), " +
                "subject VARCHAR(50), description VARCHAR(50), owner VARCHAR(50), creator VARCHAR(50), " +
                " latitude DOUBLE, longitude DOUBLE, radius DOUBLE, active INTEGER, triggered INTEGER)";
        try {
            db.execSQL(query);
        } catch(SQLException ex) {
            Log.d("Error al crear la BD", ex.toString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String query = "DROP TABLE IF EXISTS " + TABLE_NAME;
        try {
            sqLiteDatabase.execSQL(query);
            onCreate(sqLiteDatabase);
        } catch (SQLException ex) {
            Log.d("Error al actualizar", ex.toString());
        }
    }

    public ArrayList<Reminder> loadReminders(String ... whereList) throws ParseException {
        ArrayList<Reminder> reminders = new ArrayList<Reminder>();

        String where = (whereList!=null && whereList.length>0) ? whereList[0] : "";

        SQLiteDatabase sql = this.getWritableDatabase();
        Cursor cursor = sql.rawQuery("select geofence_id, subject, description, latitude, longitude, radius, owner, creator, active, triggered " +
                "FROM " + TABLE_NAME + " " + where + " ORDER BY id", null);



        if (cursor.moveToFirst()) {

            int idIndex = cursor.getColumnIndexOrThrow("geofence_id");
            int subjectIndex = cursor.getColumnIndexOrThrow("subject");
            int descriptionIndex = cursor.getColumnIndexOrThrow("description");
            int latitudeIndex = cursor.getColumnIndexOrThrow("latitude");
            int longitudeIndex = cursor.getColumnIndexOrThrow("longitude");
            int radiusIndex = cursor.getColumnIndexOrThrow("radius");
            int ownerIndex = cursor.getColumnIndexOrThrow("owner");
            int creatorIndex = cursor.getColumnIndexOrThrow("creator");
            int activeIndex = cursor.getColumnIndexOrThrow("active");
            int triggeredIndex = cursor.getColumnIndexOrThrow("triggered");
            do {
                String id = cursor.getString(idIndex);
                String subject = cursor.getString(subjectIndex);
                String description = cursor.getString(descriptionIndex);
                double latitude = cursor.getDouble(latitudeIndex);
                double longitude = cursor.getDouble(longitudeIndex);
                double radius = cursor.getDouble(radiusIndex);
                String owner = cursor.getString(ownerIndex);
                String creator = cursor.getString(creatorIndex);
                Integer activeINT = cursor.getInt(activeIndex);
                Boolean active = true;
                if(activeINT == -1)
                    active = false;
                Integer triggeredINT = cursor.getInt(triggeredIndex);
                Boolean triggered = true;
                if(triggeredINT == -1)
                    triggered = false;

                reminders.add(
                        new Reminder(
                                id,
                                subject,
                                description ,
                                latitude,
                                longitude,
                                radius,
                                owner,
                                creator,
                                active,
                                triggered));
            } while (cursor.moveToNext());
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return reminders;
    }

    // Returns the row ID of the newly inserted row, or -1 if an error occurred
    public long saveReminder(ContentValues content) {
        long result;



        SQLiteDatabase sql = this.getWritableDatabase();
        Cursor cursor = sql.rawQuery("select max(id) as m from "+TABLE_NAME,null);

        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndexOrThrow("m");
            Integer id = cursor.getInt(idIndex);
            content.put("id",id+1);
        }

        try {
            result = sql.insert(TABLE_NAME, null, content);
        } finally {
            sql.close();
        }

        return result;
    }

    public Boolean updateReminder(ContentValues content) {
        boolean success = false;

        SQLiteDatabase sql = this.getWritableDatabase();
        try {
            String geofence_id = content.get("geofence_id").toString();
            int result = sql.update(TABLE_NAME, content, "geofence_id = '" + geofence_id + "'", null);
            success = result != 0;
        } finally {
            sql.close();
        }

        return success;
    }

    public Boolean deleteReminder(String geofence_id) {
        boolean success = false;

        SQLiteDatabase sql = this.getWritableDatabase();
        try {
            int result = sql.delete(TABLE_NAME, "geofence_id = '" + geofence_id + "'", null);
            success = result != 0;
        } finally {
            sql.close();
        }

        return success;
    }

}
