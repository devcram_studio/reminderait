package reminder.devcramstudio.com.reminder.model;

/**
 * Created by nico on 29/09/15.
 */
public class Reminder {
    private String geofence_id;
    private String subject;
    private String description;
    private double latitude;
    private double longitude;
    private double radius;
    private String owner;
    private String creator;
    private boolean active;
    private boolean triggered;

    public Reminder(){
    }

    public Reminder(
            String geofence_id,
            String subject,
            String description,
            double latitude,
            double longitude,
            double radius,
            String owner,
            String creator,
            Boolean active,
            Boolean triggered) {
        this.geofence_id = geofence_id;
        this.subject = subject;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        this.owner = owner;
        this.creator = creator;
        this.active = active;
        this.triggered = triggered;
    }

    public String getGeofence_id() {
        return geofence_id;
    }

    public void setGeofence_id(String geofence_id) {
        this.geofence_id = geofence_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isTriggered() {
        return triggered;
    }

    public void setTriggered(boolean triggered) {
        this.triggered = triggered;
    }
}
