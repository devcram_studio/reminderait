package reminder.devcramstudio.com.reminder.listeners;

/**
 * Created by nico on 10/10/15.
 */
public interface OnNetworkChecked {
    void doIfExistNewtworkConnection();
    void doIfNotExistNewtworkConnection();
}
