package reminder.devcramstudio.com.reminder.syncadapter;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import reminder.devcramstudio.com.reminder.Constants;
import reminder.devcramstudio.com.reminder.R;

public class SignUpActivity extends Activity{

    private String accountType;
    private EditText emailED, passwordED;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        accountType = Constants.ACCOUNT_TYPE;

        emailED = (EditText)findViewById(R.id.email);
        passwordED = (EditText)findViewById(R.id.password);

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void, Void, Intent>() {

                    private String email;
                    private String password;

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        email = emailED.getText().toString();
                        password = passwordED.getText().toString();
                    }

                    @Override
                    protected Intent doInBackground(Void... params) {
                        final Intent resultIntent = new Intent();
                        try {
                            String authToken = ParseComServerAuthenticate.userSignUp(email, password);
                            resultIntent.putExtra(AccountManager.KEY_ACCOUNT_NAME, email);
                            resultIntent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                            resultIntent.putExtra(AccountManager.KEY_AUTHTOKEN, authToken);
                            resultIntent.putExtra(AccountManager.KEY_PASSWORD, password);
                        } catch (Exception e) {
                            resultIntent.putExtra(AccountManager.KEY_ERROR_MESSAGE, e.getMessage());
                        }

                        return resultIntent;
                    }

                    @Override
                    protected void onPostExecute(Intent intent) {
                        super.onPostExecute(intent);

                        if(intent.hasExtra(AccountManager.KEY_ERROR_MESSAGE)) {
                            Toast.makeText(SignUpActivity.this, intent.getStringExtra(AccountManager.KEY_ERROR_MESSAGE), Toast.LENGTH_SHORT).show();
                        } else {
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }

                }.execute();
            }
        });
    }

}