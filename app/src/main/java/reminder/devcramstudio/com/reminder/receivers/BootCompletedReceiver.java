package reminder.devcramstudio.com.reminder.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import reminder.devcramstudio.com.reminder.helpers.RemindersHelper;

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent arg1) {
        Log.w("boot_broadcast_poc", "starting service...");
        RemindersHelper.getInstance(context).addAllGeofences();
    }

}