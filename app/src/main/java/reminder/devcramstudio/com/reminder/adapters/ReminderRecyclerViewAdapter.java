package reminder.devcramstudio.com.reminder.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import reminder.devcramstudio.com.reminder.R;
import reminder.devcramstudio.com.reminder.activities.MainActivity;
import reminder.devcramstudio.com.reminder.activities.NewReminderActivity;
import reminder.devcramstudio.com.reminder.helpers.RemindersHelper;
import reminder.devcramstudio.com.reminder.model.Reminder;

public class ReminderRecyclerViewAdapter extends RecyclerView.Adapter<ReminderRecyclerViewAdapter.ViewHolder>{
    private List<Reminder> reminders;
    private final Context context;
    private RemindersHelper remindersHelper;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView subject;
        public TextView description;
        public View view;
        public LinearLayout rowDescription;
        public LinearLayout row;
        public CheckBox checkBox;
        public ImageButton deleteButton;

        public ViewHolder(View v) {
            super(v);
            view = v;
            subject = (TextView) v.findViewById(R.id.subject);
            description = (TextView) v.findViewById(R.id.description);
            row = (LinearLayout)v.findViewById(R.id.row_view);
            rowDescription = (LinearLayout)v.findViewById(R.id.row_description);
            checkBox = (CheckBox)v.findViewById(R.id.row_checkbox);
            deleteButton = (ImageButton)v.findViewById(R.id.delete_reminder_button);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReminderRecyclerViewAdapter(Context context, RemindersHelper remindersHelper) {
        reminders = remindersHelper.getReminders();
        this.context = context;
        this.remindersHelper = remindersHelper;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ReminderRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Reminder reminder = reminders.get(position);

        holder.subject.setText(reminder.getSubject());
        holder.description.setText(reminder.getDescription() + "(" + reminder.getLatitude() + "," + reminder.getLongitude() + ")");

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remindersHelper.removeReminder(reminder.getGeofence_id());
                ((MainActivity) context).updateReciclerView();
            }
        });

        holder.rowDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, NewReminderActivity.class);
                intent.putExtra("new", false);
                intent.putExtra("subject", reminder.getSubject());
                intent.putExtra("description", reminder.getDescription());
                intent.putExtra("longitude", reminder.getLongitude());
                intent.putExtra("latitude", reminder.getLatitude());
                intent.putExtra("radius", reminder.getRadius());
                intent.putExtra("id", reminder.getGeofence_id());
                intent.putExtra("owner", reminder.getOwner());
                intent.putExtra("creator", reminder.getCreator());

                context.startActivity(intent);
            }
        });

        holder.checkBox.setChecked(reminder.isActive());
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!((CheckBox)v).isChecked()){
                    remindersHelper.disableReminder(reminder.getGeofence_id());
                }else{
                    remindersHelper.enableReminder(reminder);
                }
                ((MainActivity) context).updateReciclerView();
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return reminders.size();
    }
}