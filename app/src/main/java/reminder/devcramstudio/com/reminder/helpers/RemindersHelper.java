package reminder.devcramstudio.com.reminder.helpers;

import android.content.ContentValues;
import android.content.Context;

import com.google.android.gms.location.Geofence;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import reminder.devcramstudio.com.reminder.model.Reminder;

/**
 * Created by nico on 4/10/15.
 */
public class RemindersHelper {
    private static RemindersHelper ourInstance = new RemindersHelper();
    private static RemindersSQLHelper remindersSQLHelper;
    private static GeofencesHelper geofencesHelper;
    private List<Reminder> reminders;

    public static RemindersHelper getInstance(Context context) {
        remindersSQLHelper = new RemindersSQLHelper(context);
        geofencesHelper.init(context);
        ourInstance.load();
        return ourInstance;
    }

    private RemindersHelper() {
        geofencesHelper = GeofencesHelper.getInstance();
    }

    public void load(){
        reminders = new ArrayList<>();
        try {
            reminders = remindersSQLHelper.loadReminders();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public List<Reminder> getReminders(){
        return reminders;
    }

    public List<Reminder> getTriggeredReminders(){
        List<Reminder> triggeredReminders = new ArrayList<>();
        for(Reminder r:reminders)
            if(r.isTriggered())
                triggeredReminders.add(r);
        return triggeredReminders;
    }

    public void addReminder(Reminder reminder){

        ContentValues values = new ContentValues();
        values.put("geofence_id", reminder.getGeofence_id());
        values.put("subject", reminder.getSubject());
        values.put("description", reminder.getDescription());
        values.put("latitude", reminder.getLatitude());
        values.put("longitude", reminder.getLongitude());
        values.put("radius", reminder.getRadius());
        values.put("owner", reminder.getOwner());
        values.put("creator", reminder.getCreator());
        values.put("active", reminder.isActive()?1:-1);
        values.put("triggered", reminder.isTriggered() ? 1 : -1);

        geofencesHelper.addGeofence(createGeofence(reminder));
        remindersSQLHelper.saveReminder(values);
        reminders.add(reminder);
    }

    public void updateReminder(Reminder reminder){
        ContentValues values = new ContentValues();
        values.put("geofence_id", reminder.getGeofence_id());
        values.put("subject", reminder.getSubject());
        values.put("description", reminder.getDescription());
        values.put("latitude", reminder.getLatitude());
        values.put("longitude", reminder.getLongitude());
        values.put("radius", reminder.getRadius());
        values.put("owner", reminder.getOwner());
        values.put("creator", reminder.getCreator());
        values.put("active", reminder.isActive() ? 1 : -1);
        values.put("triggered", reminder.isTriggered() ? 1 : -1);

        if(reminder.isActive())
            geofencesHelper.addGeofence(createGeofence(reminder));
        else
            geofencesHelper.removeGeofence(reminder.getGeofence_id());

        remindersSQLHelper.updateReminder(values);
        for(int i=0;i<reminders.size();i++)
            if(reminders.get(i).getGeofence_id().equals(reminder.getGeofence_id()))
                reminders.set(i, reminder);

    }

    public void removeReminder(String geofence_id){
        remindersSQLHelper.deleteReminder(geofence_id);
        geofencesHelper.removeGeofence(geofence_id);
        for(int i=0;i<reminders.size();i++)
            if(reminders.get(i).getGeofence_id().equals(geofence_id))
                reminders.remove(i);
    }

    public void disableReminder(String geofenceId){
        Reminder reminder = getReminder(geofenceId);
        reminder.setActive(false);
        updateReminder(reminder);
    }

    public void enableReminder(Reminder reminder) {
        reminder.setActive(true);
        updateReminder(reminder);
    }

    public Reminder getReminder(String geofence_id){
        for(Reminder r:reminders)
            if(r.getGeofence_id().equals(geofence_id))
                return r;
        return null;
    }
    public Reminder getReminder(int i){
        return reminders.get(i);
    }

    public void setTriggeredReminders(List<Geofence> triggeredGeofences) {
        for (Geofence geofence : triggeredGeofences) {
            Reminder reminder = getReminder(geofence.getRequestId());
            reminder.setTriggered(true);
            reminder.setActive(false);
            updateReminder(reminder);
        }
    }

    public void resetTriggeredReminders() {
        for(Reminder r:reminders) {
            r.setTriggered(false);
            updateReminder(r);
        }
    }

    public void addAllGeofences(){
        for(Reminder r:reminders) {
            if(r.isActive()) {
                geofencesHelper.addGeofence(createGeofence(r));
            }
        }
    }

    private Geofence createGeofence(Reminder reminder){
        Geofence geofence = new Geofence.Builder()
                .setRequestId(reminder.getGeofence_id())
                .setCircularRegion(
                        reminder.getLatitude(),
                        reminder.getLongitude(),
                        (float) reminder.getRadius()
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();

        return geofence;
    }

    public void disconnectLocationsService(){
        geofencesHelper.disconnect();
    }
}
