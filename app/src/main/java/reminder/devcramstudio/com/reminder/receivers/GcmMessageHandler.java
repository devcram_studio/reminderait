package reminder.devcramstudio.com.reminder.receivers;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;
import java.util.UUID;

import reminder.devcramstudio.com.reminder.helpers.RemindersHelper;
import reminder.devcramstudio.com.reminder.model.Reminder;

/**
 * Created by nico on 08/04/16.
 */
public class GcmMessageHandler extends IntentService {

    String mes;
    private Handler handler;
    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        handler = new Handler();
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        mes = extras.getString("title");
        showToast();
        Log.i("GCM", "Received : (" + messageType + ")  " + extras.getString("title"));

        if(mes.equals("reload")){
            final Context context = this;
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Reminder");
            query.whereEqualTo("Owner", ParseUser.getCurrentUser());
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> scoreList, ParseException e) {
                    if (e == null) {
                        for(ParseObject obj:scoreList) {
                            String subject = obj.getString("Subject");
                            String desc = obj.getString("Description");
                            Double latitude = obj.getDouble("Latitude");
                            Double longitude = obj.getDouble("Longitude");
                            Double radius = obj.getDouble("Radius");
                            Reminder reminder = new Reminder(
                                    UUID.randomUUID().toString(),
                                    subject,
                                    desc,
                                    latitude,
                                    longitude,
                                    radius,
                                    "me",
                                    "me",
                                    true,
                                    false);
                            RemindersHelper.getInstance(context).addReminder(reminder);
                            obj.deleteInBackground();
                        }
                    } else {
                        Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        }

        GcmBroadcastReceiver.completeWakefulIntent(intent);

    }

    public void showToast(){
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), mes, Toast.LENGTH_LONG).show();
            }
        });

    }
}
