package reminder.devcramstudio.com.reminder.activities;

/**
 * Created by nico on 2/10/15.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.SeekBar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import reminder.devcramstudio.com.reminder.R;
import reminder.devcramstudio.com.reminder.helpers.RemindersHelper;
import reminder.devcramstudio.com.reminder.model.Reminder;

public class NewReminderActivity extends AppCompatActivity {

    private static final int minSeekBarValue = 100;
    public GoogleMap mMap; // Might be null if Google Play services APK is not available.
    public static double locActualLatitude;
    public static double locActualLongitude;
    public static Reminder temporal ;
    public boolean isNewReminder;
    boolean quieroMiPosicion = true;
    SeekBar seekBar;
    Circle circle;
    CircleOptions circleOptions;
    String owner,creator;
    RemindersHelper remindersHelper;
    Context context;

    public double getLocActualLatitude() {
        return locActualLatitude;
    }

    public double getLocActualLongitude() {
        return locActualLongitude;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_reminder);

        context = this;

        remindersHelper = RemindersHelper.getInstance(this);

        temporal = new Reminder();

        //Get values
        Bundle bundle = getIntent().getExtras();
        isNewReminder = bundle.getBoolean("new");
        if(!isNewReminder){
            temporal.setSubject(bundle.getString("subject"));
            temporal.setLongitude(bundle.getDouble("longitude"));
            temporal.setLatitude(bundle.getDouble("latitude"));
            temporal.setRadius(bundle.getDouble("radius"));
            temporal.setGeofence_id(bundle.getString("id"));
            temporal.setOwner(bundle.getString("owner"));
            temporal.setCreator(bundle.getString("creator"));
            getSupportActionBar().setTitle("Edit Reminder");
        }else{
            creator = bundle.getString("creator");
            owner = creator;
        }
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    if (circle != null)
                        circle.remove();
                    circleOptions.center(new LatLng(temporal.getLatitude(), temporal.getLongitude()));
                    circleOptions.radius(progress + minSeekBarValue);
                    circle = mMap.addCircle(circleOptions);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                circleOptions.fillColor(0x00000000);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                circleOptions.fillColor(getResources().getColor(R.color.primary_transparent));
                if (circle != null)
                    circle.remove();
                circle = mMap.addCircle(circleOptions);
            }
        });

        circleOptions = new CircleOptions().strokeColor(getResources().getColor(R.color.primary)).fillColor(getResources().getColor(R.color.primary_transparent)).strokeWidth(8);

        setUpMapIfNeeded();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {


            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();

            mMap.clear();

            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.clear();
        mMap.setMyLocationEnabled(true);

        LatLng latLng = new LatLng(temporal.getLatitude(),temporal.getLongitude());
        final float zoom = 12;

        if(isNewReminder) {
            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    if (isNewReminder && quieroMiPosicion) {
                        mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("New reminder"));
                        temporal.setLatitude(location.getLatitude());
                        temporal.setLongitude(location.getLongitude());
                        setActualPositionDescription();
                        circleOptions.center(new LatLng(location.getLatitude(), location.getLongitude()));
                        circleOptions.radius(getSeekBarProgress());
                        circle = mMap.addCircle(circleOptions);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), zoom));
                        quieroMiPosicion = false;
                    }
                }
            });

        } else {
            mMap.addMarker(new MarkerOptions().position(latLng).title(temporal.getSubject()));
            circleOptions.center(latLng);
            seekBar.setProgress((int) temporal.getRadius());
            circleOptions.radius(getSeekBarProgress());
            circle = mMap.addCircle(circleOptions);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            setActualPositionDescription();
        }


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng));
                temporal.setLatitude(latLng.latitude);
                temporal.setLongitude(latLng.longitude);
                circleOptions.center(latLng);
                circleOptions.radius(getSeekBarProgress());
                circle = mMap.addCircle(circleOptions);
                setActualPositionDescription();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reminder_actions, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.accept:

                final EditText subjectText = new EditText(this);
                subjectText.setHint("Subject");
                subjectText.setPadding(50, 50, 50, 50);

                if(!isNewReminder) {
                    subjectText.setText(temporal.getSubject());
                    subjectText.selectAll();
                }

                final AlertDialog dialog = new AlertDialog.Builder(this)
                        .setView(subjectText)
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).create();

                dialog.show();

                subjectText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        }
                    }
                });

                dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String subject = subjectText.getText().toString();

                        if (subject.equals("")) {
                            subjectText.setHint("Subject cannot be blank");
                        } else {

                            String geofence_id;

                            if (isNewReminder)
                                geofence_id = UUID.randomUUID().toString();
                            else
                                geofence_id = temporal.getGeofence_id();

                            Reminder reminder = new Reminder(
                                    geofence_id,
                                    subject,
                                    temporal.getDescription(),
                                    temporal.getLatitude(),
                                    temporal.getLongitude(),
                                    getSeekBarProgress(),
                                    owner,
                                    creator,
                                    true,
                                    false);

                            if (isNewReminder) {
                                remindersHelper.addReminder(reminder);
                            } else {
                                remindersHelper.updateReminder(reminder);
                            }
                            dialog.dismiss();
                            finish();
                        }
                    }
                });

                return true;

            case R.id.cancel:
                finish();
                return true;
            case R.id.search:

                final EditText searchText = new EditText(this);
                searchText.setHint("Search");
                searchText.setPadding(50, 50, 50, 50);

                final AlertDialog searchDialog = new AlertDialog.Builder(this)
                        .setView(searchText)
                        .setCancelable(false)
                        .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Geocoder geo = new Geocoder(getBaseContext());
                                List<Address> gotAddresses = null;
                                try {
                                    gotAddresses = geo.getFromLocationName(searchText.getText().toString(), 1);
                                } catch (IOException e) {

                                }
                                Address address = gotAddresses.get(0);

                                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

                                String properAddress = String.format("%s, %s",
                                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                                        address.getCountryName());
                                mMap.clear();
                                mMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(address.getLatitude(), address.getLongitude())).draggable(true)
                                        .title(properAddress));
                                temporal.setLatitude(address.getLatitude());
                                temporal.setLongitude(address.getLongitude());
                                setActualPositionDescription();
                                circleOptions.center(latLng);
                                circleOptions.radius(getSeekBarProgress());
                                circle = mMap.addCircle(circleOptions);
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).create();

                searchDialog.show();

                searchText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            searchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        }
                    }
                });

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
       remindersHelper.disconnectLocationsService();
    }

    private int getSeekBarProgress(){
        return seekBar.getProgress() + minSeekBarValue;
    }

    private void setActualPositionDescription(){

        String description = "";

        Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(temporal.getLatitude(), temporal.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses!=null && addresses.size() > 0) {
            for (int i = 0; i < addresses.get(0).getMaxAddressLineIndex(); i++)
                description += addresses.get(0).getAddressLine(i) + ", ";
            description += addresses.get(0).getCountryName() + ".";
        }

        temporal.setDescription(description);
    }
}
