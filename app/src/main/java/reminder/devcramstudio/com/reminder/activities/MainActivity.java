package reminder.devcramstudio.com.reminder.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.android.supportv7.widget.decorator.DividerItemDecoration;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.software.shell.fab.ActionButton;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import reminder.devcramstudio.com.reminder.Constants;
import reminder.devcramstudio.com.reminder.R;
import reminder.devcramstudio.com.reminder.adapters.ReminderRecyclerViewAdapter;
import reminder.devcramstudio.com.reminder.helpers.NetworkHelper;
import reminder.devcramstudio.com.reminder.helpers.RemindersHelper;
import reminder.devcramstudio.com.reminder.listeners.OnNetworkChecked;
import reminder.devcramstudio.com.reminder.model.Reminder;
import reminder.devcramstudio.com.reminder.syncadapter.AuthenticatorActivity;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    protected static final String TAG = "camg";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private RemindersHelper remindersHelper;

    private RelativeLayout recyclerViewContainer;
    private RelativeLayout textIfNoReminders;
    private ActionButton fab;
    private AccountManager accountManager;

    GoogleCloudMessaging gcm;
    String regid;

    // Instance fields
    Account mAccount;

    //ContentResolver mResolver;

    public void getRegId(){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register("288447399539");
                    msg = regid;
                    Log.i("GCM",  msg);

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();

                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.d("gcm", msg + "\n");

                final String gcmId = msg;

                String username = accountManager.getAccountsByType(Constants.ACCOUNT_TYPE)[0].name+"";

                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo("email", username);
                Log.d("gcm_username",username);
                query.findInBackground(new FindCallback<ParseUser>() {
                    public void done(List<ParseUser> scoreList, ParseException e) {
                        if (e == null) {
                            if(scoreList.size()==1){
                                ParseUser user = scoreList.get(0);
                                user.put("gcmToken",gcmId);
                                user.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e!=null)
                                            e.printStackTrace();
                                    }
                                });
                            }
                        } else {
                            Log.d("score", "Error: " + e.getMessage());
                        }
                    }
                });

                /*ParseObject testObject = new ParseObject("Reminder");
                testObject.put("Subject", "bar");
                testObject.saveInBackground();*/
            }
        }.execute(null, null, null);
    }
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        accountManager = AccountManager.get(this);

        if(accountManager.getAccountsByType(Constants.ACCOUNT_TYPE).length==0){
            Intent intent = new Intent(MainActivity.this, AuthenticatorActivity.class);
            intent.putExtra("ACCOUNT_TYPE", Constants.ACCOUNT_TYPE);
            intent.putExtra("AUTH_TYPE", Constants.AUTHTOKEN_TYPE);
            intent.putExtra("IS_ADDING_NEW_ACCOUNT", true);
            startActivity(intent);
            finish();
            return;
        }

       // Log.d("sync", accountManager.getAccountsByType(Constants.ACCOUNT_TYPE)[0].name+"");

        getRegId();

        fab = (ActionButton)findViewById(R.id.fab);
        fab.setImageResource(R.mipmap.ic_add_white_24dp);
        fab.setOnClickListener(this);

        recyclerViewContainer = (RelativeLayout)findViewById(R.id.recyclerViewContainer);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        textIfNoReminders = (RelativeLayout)findViewById(R.id.noReminders);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);



    }

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                Constants.ACCOUNT, Constants.ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);

        Log.d("sync", "creating new account");
        accountManager.addAccount(Constants.ACCOUNT_TYPE, Constants.AUTHTOKEN_TYPE, null, null, this, new AccountManagerCallback<Bundle>() {
            @Override
            public void run(AccountManagerFuture<Bundle> future) {
                try {
                    Bundle bnd = future.getResult();
                    Log.d("sync", "AddNewAccount Bundle is " + bnd);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, null);
        return newAccount;
    }


    @Override
    protected void onStart() {
        super.onStart();
        //Get reminders
        remindersHelper = RemindersHelper.getInstance(this);
        remindersHelper.resetTriggeredReminders();

        // specify an adapter (see also next example)
        mAdapter = new ReminderRecyclerViewAdapter(this,remindersHelper);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        updateReciclerView();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateReciclerView();
    }

    @Override
    public void onClick(View v) {
        final Context c = v. getContext();
        if(v.getId()==fab.getId()){
            NetworkHelper.getInstance().hasActiveInternetConnection(this, new OnNetworkChecked() {
                @Override
                public void doIfExistNewtworkConnection() {
                    Intent intent = new Intent(MainActivity.this, NewReminderActivity.class);
                    intent.putExtra("new", true);
                    intent.putExtra("creator", "creator");
                    startActivity(intent);
                }

                @Override
                public void doIfNotExistNewtworkConnection() {
                    Toast.makeText(getApplicationContext(), "You need Internet connection to create a Reminder from a map. However you can create a Reminder typing its values.", Toast.LENGTH_LONG).show();
                    final EditText subject = new EditText(c);
                    subject.setHint("Subject");
                    subject.setPadding(50, 50, 50, 50);
                    final EditText latitude = new EditText(c);
                    latitude.setHint("Latitude");
                    latitude.setPadding(50, 50, 50, 50);
                    final EditText longitude = new EditText(c);
                    longitude.setHint("Longitude");
                    longitude.setPadding(50, 50, 50, 50);
                    final EditText radius = new EditText(c);
                    radius.setHint("Radius");
                    radius.setPadding(50, 50, 50, 50);
                    LinearLayout linearLayout = new LinearLayout(c);
                    linearLayout.setOrientation(LinearLayout.VERTICAL);
                    linearLayout.addView(subject);
                    linearLayout.addView(latitude);
                    linearLayout.addView(longitude);
                    linearLayout.addView(radius);

                    final AlertDialog dialog = new AlertDialog.Builder(c)
                            .setView(linearLayout)
                            .setCancelable(false)
                            .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            }).create();

                    dialog.show();

                    subject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                            }
                        }
                    });

                    dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (subject.getText().toString().equals("")) {
                                subject.setHint("Subject cannot be blank");
                            }else if(latitude.getText().toString().equals("")){
                                latitude.setHint("Latitude cannot be blank");
                            }else if(longitude.getText().toString().equals("")){
                                longitude.setHint("Longitude cannot be blank");
                            }else if(radius.getText().toString().equals("")){
                                radius.setHint("Radius cannot be blank");
                            }else {
                                Reminder reminder = new Reminder(
                                        UUID.randomUUID().toString(),
                                        subject.getText().toString(),
                                        "",
                                        Double.parseDouble(latitude.getText().toString()),
                                        Double.parseDouble(longitude.getText().toString()),
                                        Double.parseDouble(radius.getText().toString()),
                                        "owner",
                                        "creator",
                                        true,
                                        false);

                                remindersHelper.addReminder(reminder);
                                updateReciclerView();
                                dialog.dismiss();
                            }
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
    }

    public void updateReciclerView(){
        mAdapter.notifyDataSetChanged();
        if(remindersHelper.getReminders().size()==0){
            textIfNoReminders.setVisibility(View.VISIBLE);
            recyclerViewContainer.setVisibility(View.GONE);
        }else{
            textIfNoReminders.setVisibility(View.GONE);
            recyclerViewContainer.setVisibility(View.VISIBLE);
        }
    }
}
