package reminder.devcramstudio.com.reminder;

/**
 * Created by nico on 16/11/15.
 */
public class Constants {

    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "reminder.devcramstudio.com.reminder.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "devcramstudio.com";
    // The account name
    public static final String ACCOUNT = "dummyaccount";

    public static final String AUTHTOKEN_TYPE = "AUTHTOKEN_TYPE";
}
