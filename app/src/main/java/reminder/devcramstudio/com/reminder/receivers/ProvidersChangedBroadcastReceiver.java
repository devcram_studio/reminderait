package reminder.devcramstudio.com.reminder.receivers;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import reminder.devcramstudio.com.reminder.helpers.RemindersHelper;

public class ProvidersChangedBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.i("pcbr","GPS_PROVIDER IS enabled");
        } else {
            // GPS_PROVIDER is NOT enabled...
            Log.i("pcbr","GPS_PROVIDER IS NOT enabled");
        }
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // NETWORK_PROVIDER IS enabled...
            Log.i("pcbr","NETWORK_PROVIDER IS enabled");
        } else {
            // NETWORK_PROVIDER is NOT enabled...
            Log.i("pcbr","NETWORK_PROVIDER IS NOT enabled");
        }
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Log.i("pcbr","GPS_PROVIDER AND NETWORK_PROVIDER ARE enabled");
            RemindersHelper.getInstance(context).addAllGeofences();
        }
    }
}