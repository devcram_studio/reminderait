package reminder.devcramstudio.com.reminder.helpers;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import reminder.devcramstudio.com.reminder.services.GeofenceTransitionsIntentService;

/**
 * Created by nico on 2/10/15.
 */
public class GeofencesHelper {

    private static GeofencesHelper instance;
    private GoogleApiClient mGoogleApiClient;
    private Context context;
    private Geofence geofenceToAdd;
    private List<String> geofenceIdsToRemove;
    private PendingIntent intent;

    private GeofencesHelper(){
        geofenceIdsToRemove = new ArrayList<String>();
    }

    public static GeofencesHelper getInstance(){
        if(instance == null)
            return instance = new GeofencesHelper();
        else
            return instance;
    }

    public void init(Context context){
        this.context = context.getApplicationContext();
    }

    public void addGeofence(Geofence geofence){
        this.geofenceToAdd = geofence;

        if(intent==null) {
            Intent intent1 = new Intent(context, GeofenceTransitionsIntentService.class);
            intent = PendingIntent.getService(context, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        buildGoogleApiClient(connectionAddListener);
    }

    public void removeGeofence(String id){
        geofenceIdsToRemove.clear();
        geofenceIdsToRemove.add(id);
        buildGoogleApiClient(connectionRemoveListener);
    }

    public void removeGeofences(List<Geofence> geofences) {
        geofenceIdsToRemove.clear();
        for(Geofence geofence : geofences)
            geofenceIdsToRemove.add(geofence.getRequestId());
        buildGoogleApiClient(connectionRemoveListener);
    }

    private void buildGoogleApiClient(ConnectionCallbacks connectionCallbacks) {
        if(mGoogleApiClient!=null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(connectionCallbacks)
                .addOnConnectionFailedListener(connectionFailedListener)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private ConnectionCallbacks connectionAddListener = new ConnectionCallbacks() {

        @Override
        public void onConnected(Bundle bundle) {
            if(mGoogleApiClient.isConnected())
                LocationServices.GeofencingApi.addGeofences(
                        getmGoogleApiClient(),
                        Arrays.asList(new Geofence[]{geofenceToAdd}),
                        intent
                );
        }

        @Override
        public void onConnectionSuspended(int i) {

        }
    };

    private ConnectionCallbacks connectionRemoveListener = new ConnectionCallbacks() {

        @Override
        public void onConnected(Bundle bundle) {
            if(mGoogleApiClient.isConnected())
                LocationServices.GeofencingApi.removeGeofences(
                        getmGoogleApiClient(),
                        geofenceIdsToRemove);
        }

        @Override
        public void onConnectionSuspended(int i) {

        }
    };

    private GoogleApiClient.OnConnectionFailedListener connectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.e("ERROR", "Connecting to GoogleApiClient failed.");
        }
    };

    public GoogleApiClient getmGoogleApiClient(){
        return mGoogleApiClient;
    }

    public void disconnect(){
        if(getmGoogleApiClient() != null && getmGoogleApiClient().isConnected())
            getmGoogleApiClient().disconnect();
    }
}
