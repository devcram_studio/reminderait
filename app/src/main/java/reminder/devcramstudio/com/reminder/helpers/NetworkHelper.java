package reminder.devcramstudio.com.reminder.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import reminder.devcramstudio.com.reminder.listeners.OnNetworkChecked;

/**
 * Created by nico on 10/10/15.
 */
public class NetworkHelper {
    private static NetworkHelper helper = new NetworkHelper();
    private Context context;
    private OnNetworkChecked onNetworkChecked;

    public static NetworkHelper getInstance() {
        return helper;
    }

    private boolean isNetworkAvailable() {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null;
    }
    public void hasActiveInternetConnection(Context context, OnNetworkChecked onNetworkChecked) {
        this.context = context;
        this.onNetworkChecked = onNetworkChecked;
        try {
            new ChceckNetworkConnection().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
    private class ChceckNetworkConnection extends AsyncTask<Void, Void, Boolean> {
        protected Boolean doInBackground(Void... params) {
            if (isNetworkAvailable()) {
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    urlc.setConnectTimeout(1500);
                    urlc.connect();
                    return (urlc.getResponseCode() == 200);
                } catch (IOException e) {
                    Log.e("NET", "Error checking internet connection", e);
                }
                return true;
            } else {
                Log.d("NET", "No network available!");
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean)
                onNetworkChecked.doIfExistNewtworkConnection();
            else
                onNetworkChecked.doIfNotExistNewtworkConnection();
        }
    }
}
